##############################################################################
## EtherCAT Motion Control Phase Reference Line Temperature Control Box IOC configuration
##############################################################################

require ecmccfg, 7.0.1
require std 3.6.2
require calc
require essioc
require keller33x
require prlpcb

## Initiation:
epicsEnvSet("ECMC_VER" ,"7.0.1")
epicsEnvSet("NAMING", "ESSnaming")
epicsEnvSet("IOC" ,"$(IOC="PRL:Ctrl-ECAT-001")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=$(ECMC_VER="7.0.1"),stream_VER=2.8.22,EC_RATE=200,ECMC_ASYN_PORT_MAX_PARAMS=10000"

################################################################################
########Set EtherCAT bus startup timeout command added (defaults to 30seconds):

ecmcConfigOrDie "Cfg.SetEcStartupTimeout(200)"

##############################################################################
# Configure hardware:

epicsEnvSet("ECMC_SLAVE_NUM",-1)
epicsEnvShow("ECMC_SLAVE_NUM")

epicsEnvSet("TCB_NUM",1)
epicsEnvShow("TCB_NUM")

ecmcFileExist($(E3_CMD_TOP)/hw/ecmcTCB.iocsh,1)
# TCB 1
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
# TCB 2
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
# TCB 3
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
# TCB 4
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
## TCB 5
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
## TCB 6
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
#
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "$(ECMC_SLAVE_NUM)+1")
##### Load PCB
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcPCB.iocsh
#
## TCB 7
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
## TCB 8
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
## TCB 9
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
## TCB 10
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
## TCB 11
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
### TCB 12
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
### TCB 13
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
### TCB 14
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
### TCB 15
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
### TCB 16
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
### TCB 17
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
### TCB 18
#$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB.iocsh
#
#ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "$(ECMC_SLAVE_NUM)+1")
#${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=$(ECMC_SLAVE_NUM), HW_DESC=CU1521-0010"

# Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"

##############################################################################
############# Configure diagnostics:

ecmcConfigOrDie "Cfg.EcSetDiagnostics(1)"
ecmcConfigOrDie "Cfg.EcEnablePrintouts(0)"
ecmcConfigOrDie "Cfg.EcSetDomainFailedCyclesLimit(100)"
#ecmcConfigOrDie "Cfg.SetDiagAxisIndex(1)"
ecmcConfigOrDie "Cfg.SetDiagAxisFreq(2)"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

# go active
$(SCRIPTEXEC) ($(ecmccfg_DIR)setAppMode.cmd)

##############################################################################
## Loading records with correct naming
ecmcFileExist($(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh,1)
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB1_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB2_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB3_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB4_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB5_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB6_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB7_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB8_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB9_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB10_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB11_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB12_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB13_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB14_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB15_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB16_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB17_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB18_EK1501_POS)"
$(SCRIPTEXEC) $(E3_CMD_TOP)/hw/ecmcTCB_alias.iocsh, "TCB_EK1501_POS=$(TCB19_EK1501_POS)"

# Set paramaters
epicsEnvSet("P",    "PRL:")

dbLoadRecords("$(E3_CMD_TOP)/db/global_setpoint.template", "P=$(P),R=,N=1")
dbLoadRecords("$(E3_CMD_TOP)/db/global_setpoint.template", "P=$(P),R=,N=2")
iocshLoad("$(E3_CMD_TOP)/iocsh/PID_hw.iocsh", "P=$(P)")

iocInit()


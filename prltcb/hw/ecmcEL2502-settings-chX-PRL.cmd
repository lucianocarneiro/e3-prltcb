#-d /**
#-d   \brief hardware script for EL2502-settings-chX-default
#-d   \details Parmetrization Default SDO settings for EL2502
#-d   \author Anders Sandstroem
#-d   \file
#-d   \pre Environment variables needed:
#-d   \pre -ECMC_EC_SDO_INDEX "0x80n0" where n is channel 0..3
#-d   \pre -ECMC_EC_SLAVE_NUM = slave number
#-d */

#- Operation mode
#- 0 = 20Hz-20kHz period setting, 1 unit=1000 nanosecond
#- 2 = period setting, 1 unit=100 nanosecond
#- 3 = 1Hz-20kHz period setting, 1 unit=1000 nanosecond
epicsEnvSet("ECMC_EC_SDO_INDEX_OFFSET",  "0x7")
epicsEnvSet("ECMC_EC_SDO_SIZE",          "1")
epicsEnvSet("ECMC_EC_SDO_VALUE",         "3")
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},${ECMC_EC_SDO_INDEX},$(ECMC_EC_SDO_INDEX_OFFSET),$(ECMC_EC_SDO_VALUE),$(ECMC_EC_SDO_SIZE))"

#- PWM period (1Hz-20kHz)
epicsEnvSet("ECMC_EC_SDO_INDEX_OFFSET",  "0x16")
epicsEnvSet("ECMC_EC_SDO_SIZE",          "4")
epicsEnvSet("ECMC_EC_SDO_VALUE",         "1000000")
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},${ECMC_EC_SDO_INDEX},$(ECMC_EC_SDO_INDEX_OFFSET),$(ECMC_EC_SDO_VALUE),$(ECMC_EC_SDO_SIZE))"

#- Cleanup
epicsEnvUnset("ECMC_EC_SDO_INDEX_OFFSET")
epicsEnvUnset("ECMC_EC_SDO_SIZE")
epicsEnvUnset("ECMC_EC_SDO_VALUE")
